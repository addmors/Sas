#include "ssph.h"
#include "WindowsWindow.h"


#include "Sas\Events\ApplicationEvent.h"
#include "Sas\Events\MouseEvent.h"
#include "Sas\Events\KeyEvent.h"

#include "Platform/OpenGL/OpenGLContext.h"

namespace Sas {
	static bool is_GLFWInitialized = false;
	static void GLFWErrorCallback(int error, const char* description) {
		SS_CORE_ERROR("GLFW Error {0}, {1}",error, description);
	}

	Window* Window::Create(const WindowProps& props) {
		return new WindowsWindow(props);
	};

	WindowsWindow::WindowsWindow(const WindowProps& props) {
		Init(props);
	}

	WindowsWindow::~WindowsWindow() {
		ShutDown();
	};

	void WindowsWindow::Init(const WindowProps& props) {
		m_Data.Height = props.Height;
		m_Data.Width = props.Width;
		m_Data.Title = props.Title;


		SS_CORE_INFO("Create Window {0}, ({1}, {2})", props.Title, props.Width, props.Height);
		
		if (!is_GLFWInitialized) {
			int success = glfwInit();
			SS_CORE_ASSERT(success, "Could not  Init GLFW");
			glfwSetErrorCallback(GLFWErrorCallback);
			is_GLFWInitialized = true;
		}
		m_Window = glfwCreateWindow((int)props.Width, (int)props.Height, props.Title.c_str(), nullptr, nullptr);

		m_Context = new OpenGLContext(m_Window);
		m_Context->Init();

		glfwSetWindowUserPointer(m_Window, &m_Data);
		SetVSync(true);

		//Set GLFW CALLBACK
		glfwSetWindowSizeCallback(m_Window, [](GLFWwindow* window, int width, int height) {
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
			data.Height = height;
			data.Width = width;
			WindowResizeEvent event(width, height);
			data.EventCallBack(event);
			
			});

		glfwSetWindowCloseCallback(m_Window, [](GLFWwindow* window) {
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
			WindowCloseEvent event;
			data.EventCallBack(event);

			});
		glfwSetCharCallback(m_Window, [](GLFWwindow* window, unsigned int character) {
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
			KeyTypedEvent event(character, 0);
			data.EventCallBack(event);


		});
		glfwSetKeyCallback(m_Window, [](GLFWwindow* window, int key, int scancode, int action, int mode) {
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
				switch (action)
				{
					case GLFW_PRESS:
					{
						KeyPressedEvent event(key, 0, mode);
						data.EventCallBack(event);
						break;
					}
					case GLFW_RELEASE:
					{
						KeyReleasedEvent event(key, mode);
						data.EventCallBack(event);
						break;
					}
					case GLFW_REPEAT:
					{
						KeyPressedEvent event(key, 1, mode);
						data.EventCallBack(event);
						break; 
					}
				};
			});
		glfwSetMouseButtonCallback(m_Window, [](GLFWwindow* window, int button, int action, int mode) {
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
				switch (action)
				{
					case GLFW_PRESS:
					{
						MouseButtonPressedEvent event(button);
						data.EventCallBack(event);
						break;
					}
					case GLFW_RELEASE:
					{
						MouseButtonReleasedEvent event(button);
						data.EventCallBack(event);
						break;
					}
				};
			});
		glfwSetScrollCallback(m_Window, [](GLFWwindow* window, double xOffset, double yOffset) {
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
			MouseScrolledEvent event((float)xOffset, (float)yOffset);
			data.EventCallBack(event); 
			});
		glfwSetCursorPosCallback(m_Window, [](GLFWwindow* window, double xPos, double yPos) {
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
			MouseMovedEvent event((float)xPos, (float)yPos);
			data.EventCallBack(event);
			});
	};

	void WindowsWindow::ShutDown() {
		glfwDestroyWindow(m_Window);
	};

	void WindowsWindow::OnUpdate() {
		glfwPollEvents();
		m_Context->SwapBuffers();
	}

	void WindowsWindow::SetVSync(bool enable) {
		if (enable) glfwSwapInterval(1); else glfwSwapInterval(0);
		m_Data.vSync = enable;
	};
	bool WindowsWindow::IsVSync() const { return m_Data.vSync; };	


}