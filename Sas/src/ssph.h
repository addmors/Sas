#pragma once



#include <memory>
#include <utility>
#include <algorithm>
#include<iostream>
#include<sstream>
#include <functional>
#include<string>
#include<vector>

#include <unordered_map>
#include <unordered_set>
#include "Sas/Log.h"

#ifdef SS_PLATFORM_WINDOWS 
#include <Windows.h>
#endif